import random
import matplotlib.pyplot as plt
import numpy as np


class Player:
    def __init__(self, board):
        self.board = board
        self.position = 0
        self.num_moves = 0

    def make_move(self):
        self.position += random.randint(1, 6)
        if self.position in board['chutes_and_ladders']:
            self.position = board['chutes_and_ladders'][self.position]
        self.num_moves += 1


def one_game(board):
    player = Player(board)
    while player.position < board['goal']:
        player.make_move()
    return player.num_moves


def experiment(num_games, seed, board):
    random.seed(seed)
    durations = []
    for _ in range(num_games):
        num_moves = one_game(board)
        durations.append(num_moves)
    return durations


if __name__ == "__main__":

    board = {'goal': 25,
             'chutes_and_ladders': {1: 12, 13: 22, 14: 3, 20: 8}}

    durations = experiment(1000, 1710, board)

    print(f'Shortest game duration: {min(durations):4d}')
    print(f'Mean game duration    : {np.mean(durations):6.1f} ± {np.std(durations):.1f}')
    print(f'Longest game duration : {max(durations):4d}')

    hv, hb = np.histogram(durations, bins=np.arange(0, max(durations)))
    plt.figure(figsize=(8, 3))
    plt.step(hb[:-1], hv)
    plt.show()
