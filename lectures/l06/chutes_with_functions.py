import random
import matplotlib.pyplot as plt
import numpy as np


def make_move(position, num_moves):
    position += random.randint(1, 6)
    if position == 1:
        position = 12
    elif position == 13:
        position = 22
    elif position == 14:
        position = 3
    elif position == 20:
        position = 8
    num_moves += 1

    return position, num_moves


def one_game():
    num_moves = 0
    position = 0
    while position < 25:
        position, num_moves = make_move(position, num_moves)
    return num_moves


def experiment(num_games, seed):
    random.seed(seed)
    durations = []
    for _ in range(num_games):
        num_moves = one_game()
        durations.append(num_moves)
    return durations


if __name__ == "__main__":
    durations = experiment(1000, 1710)

    print(f'Shortest game duration: {min(durations):4d}')
    print(f'Mean game duration    : {np.mean(durations):6.1f} ± {np.std(durations):.1f}')
    print(f'Longest game duration : {max(durations):4d}')

    hv, hb = np.histogram(durations, bins=np.arange(0, max(durations)))
    plt.figure(figsize=(8, 3))
    plt.step(hb[:-1], hv)
    plt.show()
